# AI CTF Challenge Notebooks

This project host all the public notebooks for CTF Challenges. There are different ways to run the notebooks,

- download the notebook and run it locally or in a container.
- Run in Binder
- Run in Google Coblaboratory


## Directory Structure
|Path/File|Description|
|---|------------|
|/| base directory of project|
|/`requirements.txt`| list the python library needed for the notebook. Note that this file is shared for all challenges. |
|/`<challenge name>`| each challenge with its notebook and associated files will be placed in separate directory. Refer to the `Example` directory.|

## Running in Binder
Our preferred approach is to run the notebooks in [Binder](https://mybinder.org).

Binder will generate a URL that host the Jupyter notebook online. For each challenge, go to the [Binder](https://mybinder.org) website and do the following:

1. Select **GitLab**
2. Enter the repository URL. E.g. https://gitlab.com/oscarx7b/aictf/challenge-notebooks
3. Enter path to notebook. E.g. Example/Starting-Jupyter.ipynb
4. Copy the shareable URL. E.g. https://mybinder.org/v2/gl/oscarx7b%2Faictf%2Fchallenge-notebooks/master?filepath=Example%2FStarting-Jupyter.ipynb


![Binder Example](/images/binderexample.png)


It will take awhile when launching on Binder for the first time as it will have to perform an initial build.
